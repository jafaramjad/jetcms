package app


import (
  "net/http"
  "fmt"
  "appengine"
  "appengine/datastore"
    "appengine/capability"
    "appengine/runtime"

  //"appengine/blobstore"
  //"encoding/csv"

  "strings"
  "strconv"
  "encoding/json"
  "time"
  //"math"

)


//var queryType = "data"


/****************************GET / POST SETTINGS*********************************/
func control_settings(w http.ResponseWriter, r *http.Request) {
  
  //ADMIN
  checkAdmin(w,r)

  //CONTEXT
  c := appengine.NewContext(r)

  /*******************************GET**************************/
  if r.Method == "GET"{
  


    //QUERY
    q := datastore.NewQuery("settings").Order("-Last_Update").Limit(1)


    //DB GET ALL
    var db []*Settings 
    keys,err := q.GetAll(c,&db)

    //DB ERR
    if err != nil {
      fmt.Fprint(w,"error getting items: " + err.Error())
      return
    }

    //VAR
    var dbData []map[string]string

    //fmt.Fprintln(w,keys)

    //CHECK IF DATA EXISTS
    if db != nil {

    //FOR DB ITEMS THAT EXIST
    for i := range db {
      
      //KEYS ENCODE
      k := keys[i].Encode()

      dbData = append(dbData,
        map[string]string {
         //"title": db[i].Filename,
         "key":k,
         "site_title":db[i].Site_Title,
         "site_closed": db[i].Site_Closed,
         "site_online": db[i].Site_Online,
         "site_analytics":db[i].Site_Analytics,
         "soon_url":db[i].Soon_URL,
         "admin_email":db[i].Admin_Email,
         "cache_control":db[i].Cache_Control,
         "cache_days":db[i].Cache_Days,
         "cache_months":db[i].Cache_Months,
     

       },

      //END APPEND
      )

    //END FOR
    } 

    //ELSE NO DATA
    } else {

      dbData = append(dbData,
        map[string]string {
         "site_title":"",
         "site_closed":"",
         "site_online":"",
         "site_analytics":"",
         "soon_url":"",
         "admin_email":"",
         "cache_control":"",
         "cache_days":"",
         "cache_months":"",
         

       },
      )

    //END CHECK DATA
    }


    //RENDER CONTROL
  //renderControl(w, r, "/control/settings.html", data, dbData)
  
      //AJAX REQUEST
    if r.Header.Get("X-Requested-With") != "" {

        //MARSHAL JSON
        j,errJSON := json.Marshal(dbData)
        if errJSON != nil {
          fmt.Fprintln(w,"error with JSON")
        }

        //SET CONTENT-TYPE
        w.Header().Set("Content-Type", "application/json")

        //DISPLAY JSON
        fmt.Fprint(w,string(j))
  
    //STATIC
    } else {
        
        renderControl(w, r, "/control/settings.html", nil, dbData)

    }



/********************************POST*********************************/
} else {

      //fmt.Fprintln(w,"posted ajax:")
//GET FORM VALS
  formVal := func(val string)string{
    return r.FormValue(val)
  }
  
  //MAP FORM VALS
  dbEntry := Settings {
    Site_Title: formVal("site_title"),
    Site_Closed: formVal("site_closed"),
    Site_Online: formVal("site_online"),
    Site_Analytics:formVal("site_analytics"),
    Soon_URL:formVal("soon_url"),
    Cache_Control:formVal("cache_control"),
    Cache_Days:formVal("cache_days"),
   Cache_Months:formVal("cache_months"),
    Admin_Email: formVal("admin_email"),
    Last_Update: time.Now(),
  }
  

  //DB PUT
  key, err := datastore.Put(c, datastore.NewIncompleteKey(c, "settings", nil), &dbEntry)
    
    //IF ERRORS
    if err != nil {
        fmt.Fprint(w,"error adding")
        return

    //NO ERRORS
    } else {

  

      //FLUSH LIST
      list := []string {
         "SiteTitle",
         "SiteClosed",
         "SiteAnalytics",
         "SiteOnline",
         "SoonURL",
         "CacheControl",
         "CacheDays",
         "CacheMonths",
         "AdminEmail",
      }

      //FLUSH CACHE
      cacheFlushMulti(list,r)


      //PREP JSON
      m := map[string]string{
        "message":"new settings saved",
        "key":key.Encode(),
        "title":dbEntry.Site_Title,
        "adminSlug":AdminSlug,
      } 

      //MARSHAL JSON
      j,errJSON := json.Marshal(m)
      if errJSON != nil {
        fmt.Fprintln(w,"error with JSON")
      }

      //DISPLAY JSON
      w.Header().Set("Content-Type", "application/json")
      fmt.Fprint(w,string(j))
      return

    //END ERRORS
    }

//END POST
}

//END FUNC
}


/****************************GET SETTINGS STATS*********************************/
func control_settings_stats(w http.ResponseWriter, r *http.Request) {
  
  //ADMIN
  checkAdmin(w,r)

  //CONTEXT
  c := appengine.NewContext(r)

  /*******************************GET**************************/
  if r.Method == "GET"{
  


//GET RUNTIME STATS
  stats,_ := runtime.Stats(c)

/*
  fmt.Fprintln(w,stats.CPU.Total)
  fmt.Fprintln(w,stats.CPU.Rate1M)
  fmt.Fprintln(w,stats.CPU.Rate10M)
  fmt.Fprintln(w,stats.RAM.Current)
  fmt.Fprintln(w,stats.RAM.Average1M)
  fmt.Fprintln(w,stats.RAM.Average10M)
*/


    //DATA
     data := map[string]string{
          "cpuTotal": strconv.FormatFloat(stats.CPU.Total, 'E', -1, 64),
          "cpu1M": strconv.FormatFloat(stats.CPU.Rate1M, 'E', -1, 64),
          "cpu10M": strconv.FormatFloat(stats.CPU.Rate10M, 'E', -1, 64),
          "ramCurrent": strconv.FormatFloat(stats.RAM.Current, 'E', -1, 64),
          "ram1M": strconv.FormatFloat(stats.RAM.Average1M, 'E', -1, 64),
          "ram10M": strconv.FormatFloat(stats.RAM.Average10M, 'E', -1, 64),
     } 



      //AJAX REQUEST
    if r.Header.Get("X-Requested-With") != "" {

        //MARSHAL JSON
        j,errJSON := json.Marshal(data)
        if errJSON != nil {
          fmt.Fprintln(w,"error with JSON")
        }

        //SET CONTENT-TYPE
        w.Header().Set("Content-Type", "application/json")

        //DISPLAY JSON
        fmt.Fprint(w,string(j))
  
    
    } 


//END GET
}

//END FUNC
}


/****************************GET SETTINGS INFO*********************************/
func control_settings_info(w http.ResponseWriter, r *http.Request) {
  
  //ADMIN
  checkAdmin(w,r)

  //CONTEXT
  c := appengine.NewContext(r)

  /*******************************GET**************************/
  if r.Method == "GET"{
  




           /**********************USER INFO**********************/     
currentUser := getCurrentUser(w,r)
//currentUser := "N/A"

    //DATA
     data := map[string]string{


          "dataCenter":appengine.Datacenter(),
          "serverSoftware":appengine.ServerSoftware(),
          "versionID": strings.Split(appengine.VersionID(c),".")[0],
          "currentUser": currentUser,

     } 



      //AJAX REQUEST
    if r.Header.Get("X-Requested-With") != "" {

        //MARSHAL JSON
        j,errJSON := json.Marshal(data)
        if errJSON != nil {
          fmt.Fprintln(w,"error with JSON")
        }

        //SET CONTENT-TYPE
        w.Header().Set("Content-Type", "application/json")

        //DISPLAY JSON
        fmt.Fprint(w,string(j))
  
    
    } 


//END GET
}

//END FUNC
}

/****************************GET SETTINGS CHECKS*********************************/
func control_settings_checks(w http.ResponseWriter, r *http.Request) {
  
  //ADMIN
  checkAdmin(w,r)

  //CONTEXT
  c := appengine.NewContext(r)

  /*******************************GET**************************/
  if r.Method == "GET"{
  

  //VARS
  checkDataRead  := "checkOnline"
  checkDataWrite := "checkOnline"
  checkCache := "checkOnline"
  checkMail := "checkOnline"
  checkBlob := "checkOnline"

  checkServices := "checkOnline"


  //CHECK DB READ
  if !capability.Enabled(c, "datastore_v3", "*") {
        http.Error(w, "The Database read capability is currently unavailable.", 503)
        checkDataRead = "checkOffline"
        return
  }

  //CHECK DB WRITE
  if !capability.Enabled(c, "datastore_v3", "write") {
        http.Error(w, "The Database write capability is currently unavailable.", 503)
        checkDataWrite = "checkOffline"
        return
  } 
  
  //CHECK MAIL
  if !capability.Enabled(c, "mail", "*") {
        http.Error(w, "The Mail capability is currently unavailable.", 503)
        checkMail = "checkOffline"
        return
  } 

    //CHECK MEMCACHE
  if !capability.Enabled(c, "memcache", "*") {
        http.Error(w, "The Memcache capability is currently unavailable.", 503)
        checkCache = "checkOffline"
        return
  } 

    //CHECK BLOBSTORE
  if !capability.Enabled(c, "blobstore", "*") {
        http.Error(w, "The blobstore capability is currently unavailable.", 503)
        checkBlob = "checkOffline"
        return
  } 


  switch off := "checkOffline"; off {
	case checkDataRead:
    checkServices = "checkOuttage"
    
	case checkDataWrite:
    checkServices = "checkOuttage"
    
  case checkMail:
    checkServices = "checkOuttage"
    
  case checkCache:
    checkServices = "checkOuttage"
    
  case checkBlob:
    checkServices = "checkOuttage"
    
	default:
		// freebsd, openbsd,
		// plan9, windows...
    checkServices = "checkOnline"
	}

if  checkDataRead == "checkOffline" && 
    checkDataWrite == "checkOffline" && 
    checkMail == "checkOffline" &&
    checkCache == "checkOffline" &&
    checkBlob == "checkOffline" {

      checkServices = "checkOffline"
      

    }


    //DATA
     data := map[string]string{
        "checkServices" :checkServices,
      /*
          "checkDataRead":checkDataRead,
          "checkDataWrite":checkDataWrite,
          "checkMail":checkMail,
          "checkCache":checkCache,
          "checkBlob":checkBlob,
          */

     } 



      //AJAX REQUEST
    if r.Header.Get("X-Requested-With") != "" {

        //MARSHAL JSON
        j,errJSON := json.Marshal(data)
        if errJSON != nil {
          fmt.Fprintln(w,"error with JSON")
        }

        //SET CONTENT-TYPE
        w.Header().Set("Content-Type", "application/json")

        //DISPLAY JSON
        fmt.Fprint(w,string(j))
  
    
    } 


//END GET
}

//END FUNC
}

/****************************GET SETTINGS USERINFO*********************************/
func control_settings_userinfo(w http.ResponseWriter, r *http.Request) {
  
  //ADMIN
  checkAdmin(w,r)

  //CONTEXT
  //c := appengine.NewContext(r)

  /*******************************GET**************************/
  if r.Method == "GET"{
  




           /**********************USER INFO**********************/     
currentUser := getCurrentUser(w,r)
//currentUser := "N/A"

    //DATA
     data := map[string]string{

          "currentUser": currentUser,

     } 



      //AJAX REQUEST
    if r.Header.Get("X-Requested-With") != "" {

        //MARSHAL JSON
        j,errJSON := json.Marshal(data)
        if errJSON != nil {
          fmt.Fprintln(w,"error with JSON")
        }

        //SET CONTENT-TYPE
        w.Header().Set("Content-Type", "application/json")

        //DISPLAY JSON
        fmt.Fprint(w,string(j))
  
    
    } 


//END GET
}

//END FUNC
}