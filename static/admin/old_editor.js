 var TabItemArray = [];

 var TabItemBlank = new Object({

  data: {
    title: '',
    pagetitle: '',
    slug: '',
    description: '',
    keywords:'',
    single:'',
    content: '',
    template: ''
  },
  url: '',
  isNew: true,
  hasChanged: false

});

 TabItemArray.push(TabItemBlank);

 var myTextarea = document.getElementById("text_area_content");
 var editor = CodeMirror.fromTextArea(myTextarea, {
    lineNumbers: true,
    tabSize: 2,
    indentWithTabs: true,
    mode: 'text/html',
    autoCloseTags: {
      whenOpening: true,
      indentTags: 'none'
    }

  });

 editor.setSize("100%","100%");

/*
 //GUTTER WIDTH
var codeWidth = $(".CodeMirror-sizer").css('width');
console.log(codeWidth);
$("#editor_bottom_gutter").css('width', codeWidth);
*/



//START DOC
$(document).ready(function(){


  console.log('Welcome to the Control Room');
  //CoolClock.findAndCreateClocks();

//GLOBALS
var TabIndex;
var RowIndex;
var NewItem = true;
var myDataTypeSlug = document.getElementById("input_slug");


//KEY FILTER OVERRIDE FOR INPUTS/TEXTAREAS/SELECTS
key.filter = function(event){
  var tagName = (event.target || event.srcElement).tagName;
  key.setScope(/^(INPUT|TEXTAREA|SELECT)$/.test(tagName) ? 'input' : 'other');
  return true;
}

 //SAVE PAGE WITH KEYBOARD
 key('⌘+s, ctrl+s', function(event){ 
    event.preventDefault();
    //alert('saving page');
    var form =  $("#EditForm")[0];
    console.log(form.action);
    submitForm(form.action);
 });

//INIT TITLE LISTENER
ListenTitleChange(NewItem);

 //EDITING INPUT TITLE
//$("#input_title").on('keyup',function(event){
function ListenTitleChange(newItem){


    //$(".TabEdit.active .TabTitle").on('keydown keyup',function(event){
  //$("body").on('keydown keyup','.TabEdit.active .TabTitle',function(event){  
  $("body").on('keyup','.TabEdit.active .TabTitle',function(event){  

      //VARS
      //var title = event.target.value;
      var title = event.target.innerHTML;
      var pageTitle = $("#input_page_title")[0];
      var hiddenTitleInput = $("#input_title")[0];

      console.log(title);
      console.log(event.keyCode);

      if (event.keyCode == 13) {
        console.log("enter pressed...");
          event.stopPropagation();
          event.preventDefault();
          return false;
        }

        if(event.keyCode == 32){
          console.log("space pressed...");
          var titleSplit = title.split(" ");
          if(title.search("&nbsp;") > 0){
            event.stopPropagation();
            event.preventDefault();
            console.log("no more spaces!!")
            return false;
          }
        }
    /*
        if(event.keyCode == 8){
          console.log("backspace pressed...");
          var titleSplit = title.split(" ");
          var titleNBSP = title.search("&nbsp;");
          var titleNoSpaceAtEnd = title.substr(0,titleNBSP);

          if(titleNBSP > 0){
            //event.stopPropagation();
            //event.preventDefault();
            console.log(titleNBSP);
            console.log("no more spaces left by backspaces")
            console.log(titleNoSpaceAtEnd);


              pageTitle.value = titleNoSpaceAtEnd;
              hiddenTitleInput.value = titleNoSpaceAtEnd;
            //return false;
          } else {
                pageTitle.value = titleNoSpaceAtEnd;
              hiddenTitleInput.value = titleNoSpaceAtEnd;
          }
        }*/

      //NEW ITEM
      //if(NewItem){
      if(newItem){  
          var titleSplit = title.split(" ");
          var titleNBSP = title.search("&nbsp;");
          var titleNoSpaceAtEnd = title.substr(0,titleNBSP);


          if(titleNBSP > 0){
     
            console.log(titleNBSP);
            console.log(titleNoSpaceAtEnd);

            pageTitle.value = titleNoSpaceAtEnd;
            hiddenTitleInput.value = titleNoSpaceAtEnd;
          } else {
               pageTitle.value = title;
            hiddenTitleInput.value = title;
          }

        //UPDATE TITLES
        //pageTitle.value = title;
       // hiddenTitleInput.value = title;



        title.replace(/<(?:.|\n)*?>/gm,"");

        //MANIPULATE
        title = title.trim();
        title = title.replace(/\s/g,"-");
        title = title.toLowerCase();
        title = title.replace("&nbsp","");

        /** 
        // alphabet + numberls + hyphen
        /^[a-zA-Z0-9- ]*$/  
        **/
        //title = title.match(/^[a-zA-Z0-9- ]*$/)
        //title = title.replace(/[\W]/g, "");
        title = title.replace(/[^a-zA-Z0-9\-]/gi,"-");


        if(title.slice(-1) == "-"){
          var length = title.length;
              length = length -1;
          title = title.substr(0,length);
        } 


        title = title.replace("--","-");
        title = title.replace("---","-");
        title = title.replace("----","-");

        //ASSIGN TITLE TO SLUG
            var slug = $("#input_slug")[0];

        slug.value = "/"+title;
         


        //DEBUG
        //console.log(event);
        //console.log("inserting BR at Char " + end);
      //END NEW ITEM
      } else {

          var titleSplit = title.split(" ");
          var titleNBSP = title.search("&nbsp;");
          var titleNoSpaceAtEnd = title.substr(0,titleNBSP);


          if(titleNBSP > 0){
     
            console.log(titleNBSP);
            console.log(titleNoSpaceAtEnd);

            pageTitle.value = titleNoSpaceAtEnd;
            hiddenTitleInput.value = titleNoSpaceAtEnd;
          } else {
               pageTitle.value = title;
            hiddenTitleInput.value = title;
          }
      }

    //END EDIT INPUT TITLE
    });

//END LISTEN TITLE EDIT
}
  
//EDITING IN TEXTAREA
$("#text_area_content").on('keyup',function(event){
  
  //IF RETURN LINE
  if(event.keyCode == 13){

    //VARS
    var field = event.target;
    var br;
    var end = event.target.selectionEnd;
    var start = event.target.selectionStart;

    //SHIFT KEY
    if(event.shiftKey == true){
      event.preventDefault();
      br = "<br>\r";
    } else {
      br = "";
    }

    //CALC END CURSOR
    var end_cursor = field.value.substring(0, start).length + (br.length);


    //INSERT <BR>
    field.value = field.value.substring(0,start) + br  + field.value.substring(end,field.value.length);

    //END CURSOR
    field.focus();
    field.setSelectionRange(end_cursor,end_cursor);

    //DEBUG
    //console.log(event);
    //console.log("inserting BR at Char " + end);

  //END IF
  }

//END EDIT TEXTAREA
});


  //CLICK SUBMIT BUTTON
  $("#EditForm").on('submit',function(event){
      event.preventDefault();
      var url = event.target.action;

      //console.log(event);
      //console.log($("#EditForm").serialize());
      submitForm(url);


      return false;
  //END CLICK SUBMIT
   });

//SUBMIT FORM AJAX
function submitForm(url){


var titleVal = $("#input_title")[0].value;
var editorVal = editor.getValue();

//CHECK TITLE
if(titleVal === ""){
  //ALERT
  swal(
  'No Title!',
  'You forgot to enter a Title',
  'error'
  ).catch(swal.noop);

  return;
}

//CHECK EDITOR
if(editorVal === ""){
  //ALERT
  swal(
  'No Content!',
  'You did not enter any Content',
  'error'
  ).catch(swal.noop);

  return;
}


  //GET IDE TEXT
  myTextarea.innerHTML = editor.getValue();

  var serialData = $("#EditForm").serialize();
  var serialArrayData = $("#EditForm").serializeArray();

  console.log(serialData);
  console.log(serialArrayData);


/*
        data: {
        title: results.title,
        pagetitle: results.pagetitle,
        slug: results.slug,
        description: results.description,
        keywords:results.keywords,
        single:results.single,
        content: results.content,
        template: results.template
      },
      url: itemURL,
      rowIndex: itemRowIndex,
      isNew: false,
      hasChanged: false

      */

  //console.log($("#EditForm").serialize());

      //AJAX
      $.ajax({
         type: "POST",
         url: url,
         data: serialData,
         success: function(res){

            //alertify.set('notifier','position', 'top-right');

            //ALERT
            //alertify.success(res.message);

               swal(
                res.message,
                'Item was saved successfully!',
                'success'
              ).catch(swal.noop);
            //console.log(res.message);
         //console.log(res);


            //IF KEY
            if(res.key){
              addListItem(res);
            } else {
              updateListItem(res.title);
            }
         }
       });

//END SUBMITFORM FUNC
}

//FUNC UPDATE LIST ITEM
function updateListItem(newTitle){
           //   console.log(newTitle);

  var changedRow = $("#FullList tr:nth-child("+RowIndex+") td a");
      changedRow[0].innerHTML = newTitle;
  //console.log(changedRow);
}


//RESYNC DATA
function resyncData(dataArray){

  console.log("Resynching Data....");




//END FUNC RESYNC  
}

//FUNC ADD LIST ITEM
function addListItem(res){

    //INSERT NEW ROW/CELLS
    var newRow = $("#FullList")[0].insertRow(0);
    var titleCell = newRow.insertCell(0);
    var urlCell = newRow.insertCell(1);
    var deleteCell = newRow.insertCell(2);
        deleteCell.setAttribute("class","deleteItemFullTD");


    //SETUP INFO
    var titleText  = res.title;
    var titleURL   = '/'+res.adminSlug+'/pages/edit/'+res.key;
    var deleteURL  = '/'+res.adminSlug+'/pages/delete/'+res.key;
    var itemURL = res.url;

    //PREP TITLE ELEMENT
    var titleEl = document.createElement('a');
        titleEl.setAttribute("href",titleURL);
        titleEl.setAttribute("class","listItem");
        titleEl.innerHTML = titleText;

        //PREP TITLE ELEMENT
    var urlEl = document.createElement('a');
        urlEl.setAttribute("href",itemURL);
        urlEl.setAttribute("target","_blank");
        urlEl.setAttribute("class","viewItemFullTD");
        urlEl.innerHTML = "[^]";

    //PREP DELETE ELEMENT
    var deleteEl = document.createElement('a');
        deleteEl.setAttribute("href",deleteURL);
        deleteEl.setAttribute("class","deleteItemFull");
        deleteEl.innerHTML = "[x]";

    //APPEND ELEMENTS
    titleCell.appendChild(titleEl);
    urlCell.appendChild(urlEl);
    deleteCell.appendChild(deleteEl);

    //UPDATE FORM ACTION
    var form = $("#EditForm");
    form.attr('action',titleURL);


           

}


////////////////////////CLICKS/////////////////////////////

//CLICK EDIT TAB
$("body").on('click',".TabEdit",function(event){
    
  //  event.preventDefault();

      var tabBtn = event.target.parentElement;
      var tabList = event.target.parentElement.parentElement;
      var tabNumClicked = $(tabBtn).attr('data-order');


      $(".TabEdit").removeClass('active');
      $(tabBtn).addClass('active');

      if (tabNumClicked === undefined){
          return;

      } else {


        var tabData = TabItemArray[tabNumClicked].data;
        var tabURL = TabItemArray[tabNumClicked].url;
        var tabItemRow = TabItemArray[tabNumClicked].rowIndex;

        //RESET
        ResetForm();



        //GRAB ROW INDEX
        RowIndex = Number(tabNumClicked);
        
        //CHANGE FORM URL
        $("#EditForm").attr('action',tabURL);

        //UPDATE ROW INDEX
        RowIndex = tabItemRow;



        //LOAD ITEM
        LoadItem(tabData,tabURL, tabItemRow, true);


      }





      console.log(tabNumClicked);
      console.log(TabItemArray[tabNumClicked]);
      //console.log(tabList);
      //console.log(event);
      console.log("clicked Edit Tab...");






});


//CLICK ADD BUTTON
$("#AddButton a").on('click',function(event){
    event.preventDefault();
    //var url = event.target.href;

    NewItem = true;

    ResetForm();

/*
       //HIDE FIELDS/DISPLAY LIST
      $("#ListShell").show();
      $("#FIELDS_BOX").hide();
      $("#OPTIONS_BOX").hide();
      $("#Sidebar").hide();
      $("#PageBar").hide();
          $("#sectionContent").hide();
          */


});

//CLICK ADD BUTTON
$("#AddNewButton a").on('click',function(event){
    event.preventDefault();
    //var url = event.target.href;
    NewItem = true;
  myDataTypeSlug.disabled = false;
  myDataTypeSlug.style.background = "#FFF";

    ResetForm();
/*

       $("#ListShell").hide();
    $("#FIELDS_BOX").show();
    $("#OPTIONS_BOX").show();
    $("#Sidebar").show();
    $("#PageBar").show();
    $("#sectionContent").show();
          */


});

//CLICK LIST BUTTONS
$("#List, #FullList").on('click','tr td a.listItem',function(event){

    //PREVENT DEFAULT
    event.preventDefault();

    //RESET FORM
    ResetForm();

    //GRAB URL
    var url = event.target.href;

    //GRAB ROW INDEX
    RowIndex = event.target.parentElement.parentElement.rowIndex + 1;
    //console.log(RowIndex);

    //CHANGE FORM URL
    $("#EditForm").attr('action',url);


    //AJAX LOAD SINGLE ITEM
    $.ajax({
      url:url,
      success:function(results){
        LoadItem(results, url, RowIndex, false);

        //RESET AND AUTOSIZE INPUT
       // $("#input_title").css('width','80px');
        //$("#input_title").trigger('change');

      }
    //END AJAX 
    });

//END CLICK LIST
})

//CLICK DELETE BUTTONS
$("#FullList").on('click',"tr td a.deleteItemFull",function(event){
      
      //PREVENT DEFAULT
      event.preventDefault();
      
      //GRAB URL
      var url = event.target.href;

      //GRAB ROW INDEX
      var index = Number(event.target.parentNode.parentNode.rowIndex);
      //console.log(index);


swal({
  title: 'Delete Item',
  text: "Are you sure you want to delete the item?",
  type: 'error',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!',
  cancelButtonText: 'No, cancel!',
  confirmButtonClass: 'btn btn-success',
  cancelButtonClass: 'btn btn-danger',
  buttonsStyling: true

}).then(function () {

  //AJAX
        $.ajax({
           type: "POST",
           url: url,
           success: function(res){
              //RESET FORM              
              ResetForm(); 
              
              //DELETE ROW
              $("#FullList")[0].deleteRow(index);
               
              //ALERT
              //alertify.success(res);
                swal(
                  'Deleted!',
                  res,
                  'success'
                );
           }

        //END AJAX
        });


}).catch(swal.noop);

/*
, function(dismiss){

    if (dismiss === 'cancel') {
    swal(
      'Cancelled',
      'Your item was not deleted',
      'error'
    )
  }
  }
  */


/*
    //CONFIRM SETUP
    alertify.set({ 
      labels: {
        ok:"Yes",
        cancel:"No"
      },
      buttonReverse:true

    });

    //CONFIRM
    alertify.confirm("Are you sure you want to delete the item?", function(e){

      //YES
      if(e){

        //AJAX
        $.ajax({
           type: "POST",
           url: url,
           success: function(res){
              //RESET FORM              
              ResetForm(); 
              
              //DELETE ROW
              $("#FullList")[0].deleteRow(index);
               
              //ALERT
              alertify.success(res);
           }

        //END AJAX
        });
      }

    //END CONFIRM       
    });
      */

       

  //END CLICK
  }); 


//FUNC RESET ITEMS
function ResetForm(){
  var form = $("#EditForm");
      form.attr('action','/{{.Globals.AdminSlug}}/pages/list/');
      form[0].reset();

  var textareaItem = $("#EditForm textarea[name='content']");
     textareaItem[0].innerHTML = "";

  editor.setValue("");

}

//FUNC LOAD ITEMS
function LoadItem(results, itemURL, itemRowIndex, isTabClick){

 /*

         //HIDE LIST/DISPLAY FIELDS
    $("#ListShell").hide();
    $("#FIELDS_BOX").show();
    $("#OPTIONS_BOX").show();
    $("#Sidebar").show();
    $("#PageBar").show();
    $("#sectionContent").show();*/
  
  console.log(results);


//IF TAB CLICKED
if(!isTabClick){

    var TabItem = new Object({

      data: {
        title: results.title,
        pagetitle: results.pagetitle,
        slug: results.slug,
        description: results.description,
        keywords:results.keywords,
        single:results.single,
        content: results.content,
        template: results.template
      },
      url: itemURL,
      rowIndex: itemRowIndex,
      isNew: false,
      hasChanged: false

    });

    console.log("Tab Item URL: " + itemURL);

    console.log("Tab Item Row Index: " + itemRowIndex);



    /*

        $(".TabEdit").each(function(i,v){

           //console.log(v);

          //if(i > 0){
            v.classList.remove('active');
          //}
         

        });*/

    //TURN OFF LISTENER
    $("body").off('keyup','.TabEdit.active .TabTitle');
    //$(".TabEdit.active").off('keydown keyup');


    //REMOVE ACTIVE
    $(".TabEdit").removeClass('active');


    //CLONE + APPEND + MAKE ACTIVE
    $(".TabEdit").last().clone().addClass('active').appendTo( "#TabItems" );

    //PUSH OBJECT TO ARRAY
    TabItemArray.push(TabItem);

      //RELISTEN
    ListenTitleChange(false);

    console.log(TabItemArray);

//END IF IS TAB CLICK
}

/*
    for(var v in TabItemArray){
       console.log(v);
    }
*/





/*
    var TabItem = new Vue({
  el: '.TabEdit',
  delimiters: ['${', '}'],
  data: {
    title: results.title,
    pageTitle: results.pagetitle,
    url: results.slug,
    description: results.description,
    keywords:results.keywords,
    single:results.single,
    content: results.content,
    template: results.template

  }
});

    TabItemArray.push(TabItem);
*/


  
    $("#EditForm .control_forms").each(function(i,v){
      //console.log(v.name);
      var formName = v.name;
      for(var r in results){

     //   console.log(r)




        if(r == formName){
          //console.log(r + " = "+formName)
          
          if(formName == 'content'){
            var textareaItem = $("#EditForm textarea[name='"+formName+"']");
           textareaItem[0].innerHTML = results[r];
          
          //myTextarea.textContent = results[r];

          }else if(formName == 'single'){
            var inputItem = $("#EditForm input[name='"+formName+"']");
                if(results[r] == "1"){
                  inputItem[0].checked = true;
                  //inputItem[0].value = results[r];

                } else {
                  inputItem[0].checked = false;
                  //inputItem[0].value = results[r];

                }

          } else if(formName == 'title'){

            console.log(results[r]);
            var _TE = $(".TabEdit.active .TabTitle");
           _TE[0].innerHTML = results[r];
           // _TE[0].value = results[r];

            var inputItem = $("#EditForm input[name='title']");
            inputItem[0].value = results[r];


          } else {
            var inputItem = $("#EditForm input[name='"+formName+"']");
            inputItem[0].value = results[r];

          }
        
        //END IF  
        }

      //END FOR
      }

    //END EACH
    });

      console.log(myTextarea.textContent);
      //IDE SET VALUE
      editor.setValue(myTextarea.textContent);

 NewItem = false;
    //myDataTypeSlug.disabled = true;
  myDataTypeSlug.style.background = "#dcdcdc";


      $(".TabEdit").each(function(key,val){
          console.log(key);
          var item = $(this)[0];
          $(this).attr("data-order",key);
          console.log(item);
      });

  //END FUNC LOAD ITEMS
  }



  //CLICK DELETE BUTTONS
$("#editor_bottom_gutter").on('click',function(event){
      
      //PREVENT DEFAULT
      event.preventDefault();

      var lineCount = editor.lineCount();
      editor.focus();

      editor.setCursor(lineCount);

    });

//END DOC
});

